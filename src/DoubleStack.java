import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

// kasutatud materjalid:
// https://enos.itcollege.ee/~jpoial/algoritmid/adt.html
// https://www.w3resource.com/java-exercises/collection/java-collection-linked-list-exercise-24.php

public class DoubleStack {

   private LinkedList<Double> list = new LinkedList<>();

   public static void main (String[] argum) {

      // System.out.println(interpret("5 7 - 10 2 / +"));

      // System.out.println(interpret("5 2 - 7"));

      // System.out.println(interpret("67 xxx +"));

      // System.out.println(interpret("3 4 + - 5"));

      // System.out.println(interpret(""));

      System.out.println(interpret("2 5 SWAP -"));  // 3.0
      System.out.println(interpret("2 5 9 ROT - +"));  // 12.0
      System.out.println(interpret("3 DUP *"));  // 9.0
      System.out.println(interpret("-3 -5 -7 ROT - SWAP DUP * +"));  // 21.0

   }

   DoubleStack() {
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack clone = new DoubleStack();

      for (int i = (list.size() - 1); i >= 0; i--)
         clone.push(list.get(i));

      return clone;
   }

   public boolean stEmpty() {
      return list.isEmpty();
   }

   public void push (double a) {
      list.addFirst(a);
   }

   public double pop() {
      if (stEmpty())
         throw new IndexOutOfBoundsException(" stack underflow");
      double number = list.getFirst();
      list.removeFirst();
      return number;
   }

   public void op (String s) {
      if (list.size() < 1) {
         throw new IndexOutOfBoundsException(" too few elements for " + s);
      }
      double b = list.pop();
      if (s.equals("DUP")) {
         list.push(b);
         list.push(b);
      } else {
         double a = list.pop();

         switch (s) {
            case "SWAP":
               list.push(b);
               list.push(a);
               break;
            case "ROT":
               try {
                  double third = list.pop();
                  list.push(a);
                  list.push(b);
                  list.push(third);
                  break;
               } catch (NoSuchElementException e) {
                  throw new RuntimeException("Not enough elements to rotate the list " + s);
               }

            case "+":
               list.push(b + a);
               break;
            case "-":
               list.push(a - b);
               break;
            case "*":
               list.push(b * a);
               break;
            case "/":
               list.push(a / b);
               break;
            default:
               throw new RuntimeException("Unexpected argument: " + s);
         }


      }
   }

   public double tos() {
      if (stEmpty())
         throw new IndexOutOfBoundsException(" stack underflow");
      return list.getFirst();
   }

   @Override
   public boolean equals (Object o) {

      DoubleStack tmp = (DoubleStack) o;

      if (list.size() != tmp.list.size()) {
         return false;
      }
      int x = 0;
      for (Double number : list) {
         if (!number.equals(tmp.list.get(x))) {
            return false;
         }
         x++;
      }
      return true;
   }

   @Override
   public String toString() {
      StringBuilder string = new StringBuilder();

      for (int x = (list.size() - 1); x >= 0; x--) {
         string.append(list.get(x));
      }
      return string.toString();
   }

   public static double interpret (String pol) {

      // check for missing expression
      if (pol == null || pol.equals("") || pol.equals(" ")) {
         throw new RuntimeException("Missing expression!" + pol);
      }

      StringTokenizer tokens = new StringTokenizer(pol, " \t");

      DoubleStack ds = new DoubleStack();

      int x = 1;
      int tokenCount = tokens.countTokens();
      while (tokens.hasMoreElements()) {
         String token = (String) tokens.nextElement();

         if (token.equals("-") || token.equals("+") || token.equals("/") || token.equals("*") ||
                 token.equals("SWAP") || token.equals("ROT") || token.equals("DUP"))
         {
            try { // check for not enough numbers
               ds.op(token);
            } catch (NoSuchElementException e) {
               throw new RuntimeException("Not enough numbers: " + pol);
            }

         } else {
            // check for too many numbers
            if (tokenCount == x && x > 2) // When we have more than two elements we assume operator is last one
               throw new RuntimeException("Too many numbers: " + pol);

            try { // check for illegal symbols
               double tmp = Double.parseDouble(token);
               ds.push(tmp);
            } catch (NumberFormatException e) {
               throw new RuntimeException("Illegal symbol '" + token + "' in " + pol);
            }
         }
         x++;
      }

      return ds.tos();
   }

}

